#!/usr/bin/env node
/* jshint node: true, esnext: true, trailing: true */
var async = require('async'),
    cheerio = require('cheerio'),
    docopt = require('docopt').docopt,
    request = require('request'),
    bot = require('nodemw');

var fetchq = [],
    seen = [],
    mw = new bot('config.json'),
    images = {
      "wordle.png": 'Green tourism wordle.png',
      "124781224.jpg": 'DofE-group.jpg',
      "109014314.jpg": 'Tourists_tiptoe_among_Adélie_Penguins_(5913941425).jpg|320px',
      "158129801.jpg": 'Scheidegg3.jpg|320px'
    };
const BASEURL = 'http://showcase.uhi.ac.uk/resources/GEO-green-tourism/',
    BASEPAGE = 'Green tourism';

String.prototype.capitalize = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
};

function fetcher(cb) {
  var url = fetchq.shift();
  console.log(`URL: ${url}`);
  seen.push(url);
  request(`${BASEURL}${url}`, function (error, response, body) {
    console.log(`fetcher request: ${response.statusCode}`);
    if (!error && response.statusCode === 200) {
      console.log('got the body');
      cb(null, url, body);
    } else {
      console.log(`fetcher error: ${error}`);
      cb(error);
    }
  });
}

function parser(url, html, cb) {
  var next, $article, article,
      $ = cheerio.load(html);
  next = $('.btn-fwd').first().find('a').attr('href');
  console.log(`next: ${next}`);
  if (seen.indexOf(next) > -1) {
    console.log('already seen next page');
  } else {
    fetchq.push(next);
  }
  $article = $('article').first();
  $article.find('div.columns, div.row').each(function() {
    $(this).replaceWith($(this).contents());
  });
  $('div.quote').each(function() {
    var $author = $(this).find('strong').last(),
        author = $author.contents();
    $author.remove();
    $(this).replaceWith(`{{Quote|text=${$(this).contents()}|sign=${author}}}`);
  });
  $('div.reading').each(function() {
    var icon = $(this).find('i').first().attr('class'),
        kind = (icon === 'icon-globe') ? 'Web resources' : 'Reading';
    $(this).replaceWith(`{{IDevice
|type=${kind}
|body=${$(this).contents()}
}}
`);
  });
  $article.find('p').each(function() {
    if ($(this).hasClass('copyright')) {
      $(this).remove();
    } else {
      $(this).replaceWith("\n" + $(this).contents());
    }
  });
  $article.find('i').remove();
  $article.find('a').each(function() {
    var href = $(this).attr('href'),
        contents = $(this).contents();
    if (/https?:\/\/youtu((\.be)|(be\.com)).*/i.test(href)) {
      $(this).replaceWith('{{YouTube|id=|target=_blank}}');
    } else {
      $(this).replaceWith(`[${href} ${contents}]`);
    }
  });
  $article.find('.imageright').each(function() {
    $(this).find('img').attr('data-position', 'right');
    $(this).replaceWith($(this).contents());
  });
  $article.find('img').each(function() {
    var src = $(this).attr('src'),
        alt = $(this).attr('alt'),
        right = $(this).attr('data-position');
    right = (right) ? '|right' : '';
    if (src.lastIndexOf('/') > -1) {
      src = src.slice(src.lastIndexOf('/')+1);
    }
    // FIXME really should check for file existence
    if (images.hasOwnProperty(src)) {
      src = images[src];
    }
    $(this).replaceWith(`[[File:${src.capitalize()}${right}|${alt}]]`);
  });
  article = $article.html();
  // FIXME replacing \s* isn't working, so replace tabs, then spaces
  article = article.replace(/\t/g, ' ')
    .replace(/ *<\/?h1>/g, '=')
    .replace(/<\/?em>/g, "''")
    .replace(/<\/?strong>/g, "'''")
    .replace(/<!-- Instance.*-->/g, '')
    .replace(/^ *\[\[File:/mg, '[[File:')
    .replace(/&#x201[89];/g, "'")
    .replace(/&#x2013;/g, "&mdash;")
    .replace(/- /g, '-')
    .replace(/ *<div class="clear"><\/div>/g, '{{Clear}}')
    .replace(/^ +/gm, '')
    .replace(/\n\n\n+/g, "\n\n");
  cb(null, url, article);
}

function urlToPage(url) {
  if (url === 'index.html') {
    page = 'Home';
  } else {
    page = url.replace('.html', '').replace('-', ' ').capitalize();
  }
  return page;
}

function writer(url, wikitext, cb) {
  var page = urlToPage(url);
  console.log(`====== ${url} -> ${page} ======`);
  console.log(wikitext);
  mw.edit(`${BASEPAGE}/${page}`, wikitext, 'import course from University of the Highlands and Islands', cb);
}

function processPage(cb) {
  async.waterfall([
      fetcher,
      parser,
      writer
      ], cb);
}

function writeOutline() {
  var li = "An open course from [http://uhi.ac.uk/ University of the Highlands and Islands]\n\n";
  seen.forEach(function(url) {
    var page = urlToPage(url);
    li += `* [[${BASEPAGE}/${page}|${page}]]\n`;
  });
  mw.edit(`${BASEPAGE}/ Outline`, li, 'course outline', function(err) {
    if (err) {
      console.log(`Error writing outline: ${err}`);
    }
    console.log('outline written');
  });
}
// main
mw.logIn(function() {
  console.log('logged in to wiki');
  fetchq.push('index.html');

  async.whilst(
    function () {
      return fetchq.length > 0;
    },
    processPage,
    function (err) {
      if (err) {
        console.log('Error:', err);
        throw err;
      }
      writeOutline();
    }
  );
});

