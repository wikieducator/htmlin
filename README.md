htmlin
======

htmlin is a demonstration script that takes small, regularly
formatted HTML pages and imports them into Mediawiki.

> *htmlin is not intended to be used as a catch-all script, but as
a demonstration of how a script can be tailored to a particular
input format.*

As written, it:

* reads an HTML page
* parses the page to
 * convert a subset of HTML to wikitext
 * map image URLs to wiki File: pages
 * extract the URL of the "next" page in the HTML
* after it has seen all the pages, it writes a list
of the wikipages to **BASEPAGE/ Outline**

Installation
------------

The script uses [iojs](https://iojs.org/) and takes advantages
of the ES6 (AKA ES2015) advances on JavaScript.

Install iojs.  One of the easiest ways is to take advantage
of [nvm](https://github.com/creationix/nvm).

With iojs installed:
```
npm install
```

Configuration
-------------

Copy `config.example.json` to `config.json` and adjust the
parameters to suit your wiki. Edit the script to adjust
settings like the **images** map of HTML to wiki media names
and the **BASEURL** and **BASEPAGE** names pointing to the
starting place on the web and in the wiki.

Run
---

```
iojs htmlin
```

